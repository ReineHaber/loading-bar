﻿using System;

class Program
{
    public static void bar(int total , int current)
    {
        int progress = (int) Math.Floor(((double)current / (double)total)*100);
        int pros = (int)Math.Floor(((double)current / (double)total) * 50);
        Console.Write(" Loading: [");
        for(int i = 0; i < 50; i++)
        {
            if (i < pros)
            {
                Console.Write('=');
            }
            else
            {
                Console.Write(' ');
            }

        }
        Console.Write($"]{progress}%\r");
    }

    static void Main(string[] args)
    {
        int total = 100;
        for(int i =1;i <= total; i++) {
            bar(total, i);
            Thread.Sleep(1000);
        }
    }
}
